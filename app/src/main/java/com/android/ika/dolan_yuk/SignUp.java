package com.android.ika.dolan_yuk;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by user on 2/20/2018.
 */

public class SignUp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
    }
    public void signup(View view) {
        Intent intent = new Intent( this , DetailMenu.class);
        startActivity(intent);
    }

}
